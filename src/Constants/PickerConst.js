const BASE = 0;
const ACCENT = 1;
const ACCESSORIES = 2;

const MIRROR = 100;
const NIGHT = 101;
const LUX = 102;
const FOREST = 103;
const RETRO = 104;