import React from 'react';
import Espresso from './Components/Espresso';
import Picker from './Components/Picker';
import './App.css';

function App() {
  return (
    <div id='parent'>
      <div id='espresso-container'>
        <Espresso></Espresso>
      </div>
      <div id='picker-container'>
        <Picker></Picker>
      </div>
    </div>
  );
}

export default App;
