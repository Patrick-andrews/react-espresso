import React, { Component } from 'react';

export default class EspressoPart extends Component {

    render() {
        return (
            <React.Fragment>
                <img src={require(`../img/${this.props.src}`)} style={this.imgStyle()} alt=''/>
            </React.Fragment>
        )
    }

    // Set the image style
    imgStyle = () => {
        return {
            top: '10%',
            left: '10%',
            position: 'absolute',
            maxWidth: '80%',
            maxHeight: '80%'
        };
    }
}
