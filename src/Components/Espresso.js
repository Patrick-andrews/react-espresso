import React, { Component } from 'react'

import EspressoPart from './EspressoPart';

export default class Espresso extends Component {
    render() {
        return (
            <React.Fragment>
                <EspressoPart src='espresso_base.png' color=''></EspressoPart>
                <EspressoPart src='espresso_metal.png' color=''></EspressoPart>
                <EspressoPart src='espresso_plastic.png' color=''></EspressoPart>
                <EspressoPart src='espresso_knob.png' color=''></EspressoPart>
                <EspressoPart src='espresso_steam.png' color=''></EspressoPart>
                <EspressoPart src='espresso_group.png' color=''></EspressoPart>
                <EspressoPart src='espresso_controls.png' color=''></EspressoPart>
                <EspressoPart src='espresso_handle.png' color=''></EspressoPart>
            </React.Fragment>
        )
    }
}
