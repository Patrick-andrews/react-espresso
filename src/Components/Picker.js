import React, { Component } from 'react'

export default class Picker extends Component {
    
    handleChange = (force, val) => {

    }

    render() {
        return (
            <div>
                <div style={{ margin: 'auto' }}>
                    Base:
                <select onChange={this.handleChange(0)} style={{ marginLeft: '10px' }}>
                        <option value="mirror">Mirror</option>
                        <option value="night">Night</option>
                        <option value="modern">Modern</option>
                        <option value="forest">Forest</option>
                        <option value="retro">Retro</option>
                    </select>
                </div>
                <div style={{ margin: 'auto', marginTop: '10px' }}>
                    Accent:
                <select onChange={this.handleChange(1)} style={{ marginLeft: '10px' }}>
                        <option value="mirror">Mirror</option>
                        <option value="night">Night</option>
                        <option value="modern">Modern</option>
                        <option value="forest">Forest</option>
                        <option value="retro">Retro</option>
                    </select>
                </div>
                <div style={{ margin: 'auto', marginTop: '10px' }}>
                    Accessories:
                <select onChange={this.handleChange(2)} style={{ marginLeft: '10px' }}>
                        <option value="mirror">Mirror</option>
                        <option value="night">Night</option>
                        <option value="modern">Modern</option>
                        <option value="forest">Forest</option>
                        <option value="retro">Retro</option>
                    </select>
                </div>
            </div>
        )
    }
}
